use sdl2::audio::{AudioCallback, AudioSpecDesired};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

use std::collections::HashMap;
use std::time::Duration;

#[derive(Copy, Clone, Eq, PartialEq)]
enum Note { C, Cs, D, Ds, E, F, Fs, G, Gs, A, As, B }
use Note::*;

const VOLUME: f32 = 0.1;

struct SquareWave {
    phase_inc: f32,
    phase: f32,
}

impl AudioCallback for SquareWave {
    type Channel = f32;

    fn callback(&mut self, out: &mut [f32]) {
        for x in out.iter_mut() {
            *x = if self.phase <= 0.5 {
                VOLUME
            } else {
                -VOLUME
            };
            self.phase = (self.phase + self.phase_inc) % 1.0;
        }
    }
}

struct XorMod {
    child_waves: HashMap<Keycode, SquareWave>,
}

impl AudioCallback for XorMod {
    type Channel = f32;

    fn callback(&mut self, out: &mut [f32]) {
        let child_results: Vec<_> = self.child_waves.values_mut().map(|c| {
            let mut res = vec![0.0; out.len()];
            c.callback(&mut res);
            res
        }).collect();

        for (i, out_v) in out.iter_mut().enumerate() {
            let mut out_bool = false;

            for child_r in &child_results {
                out_bool ^= child_r[i] > 0.0;
            }

            *out_v = if out_bool {
                VOLUME
            } else {
                -VOLUME
            };
        }
    }
}

fn note_to_freq(note: Note, octave: i8) -> f32 {
    let freq = match note {
        C => 261.63,
        Cs => 277.18,
        D => 293.66,
        Ds => 311.13,
        E => 329.63,
        F => 349.23,
        Fs => 369.99,
        G => 392.0,
        Gs => 415.3,
        A => 440.0,
        As => 466.16,
        B => 493.88,
    };
    freq * (2.0_f32).powi(octave as i32 - 4)
}

fn keycode_to_note(kc: Keycode) -> Option<(Note, i8)> {
    match kc {
        Keycode::Z => Some((C, 4)),
        Keycode::S => Some((Cs, 4)),
        Keycode::X => Some((D, 4)),
        Keycode::D => Some((Ds, 4)),
        Keycode::C => Some((E, 4)),
        Keycode::V => Some((F, 4)),
        Keycode::G => Some((Fs, 4)),
        Keycode::B => Some((G, 4)),
        Keycode::H => Some((Gs, 4)),
        Keycode::N => Some((A, 4)),
        Keycode::J => Some((As, 4)),
        Keycode::M => Some((B, 4)),
        Keycode::Comma => Some((C, 5)),
        Keycode::Q => Some((C, 5)),
        Keycode::Num2 => Some((Cs, 5)),
        Keycode::W => Some((D, 5)),
        Keycode::Num3 => Some((Ds, 5)),
        Keycode::E => Some((E, 5)),
        Keycode::R => Some((F, 5)),
        Keycode::Num5 => Some((Fs, 5)),
        Keycode::T => Some((G, 5)),
        Keycode::Num6 => Some((Gs, 5)),
        Keycode::Y => Some((A, 5)),
        Keycode::Num7 => Some((As, 5)),
        Keycode::U => Some((B, 5)),
        Keycode::I => Some((C, 6)),
        _ => None,
    }
}

fn keycode_to_freq(kc: Keycode) -> Option<f32> {
    keycode_to_note(kc).map(|(n, o)| note_to_freq(n, o))
}

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let audio_subsystem = sdl_context.audio().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let desired_spec = AudioSpecDesired {
        freq: Some(44100),
        channels: Some(1),
        samples: None, // default sample size
    };

    let mut sampling_freq = 44100;
    let mut device = audio_subsystem.open_playback(None, &desired_spec, |spec| {
        sampling_freq = spec.freq;
        XorMod {
            child_waves: HashMap::new(),
        }
    }).unwrap();
    device.resume();

    let _window = video_subsystem.window("xor", 640, 480).build().unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} => {
                    break 'running
                },
                Event::KeyDown { keycode: Some(kc), .. } => {
                    if let Some(f) = keycode_to_freq(kc) {
                        let mut callback = device.lock();
                        let square = SquareWave {
                            phase_inc: f / sampling_freq as f32,
                            phase: 0.,
                        };
                        callback.child_waves.entry(kc).or_insert(square);
                    }
                },
                Event::KeyUp { keycode: Some(kc), .. } => {
                    let mut callback = device.lock();
                    callback.child_waves.remove(&kc);
                }
                _ => {}
            }
        }
        std::thread::sleep(Duration::from_millis(10));
    }
}
